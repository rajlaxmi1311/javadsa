import java.io.*;
class Demo{

	public static void main(String [] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());

		char ch= 'A';
		
		for(int i= 1; i<= row/2;i++){
			for(int j=1; j<=row; j++){
			System.out.print(ch +"\t");
			ch++;
		

		}
		
		System.out.println();

		ch--;
		for(int j= 1; j<= row; j++){
			System.out.print(ch +"\t");
			ch--;

		

		}
		ch++;
		System.out.println();
	
		}
	}
}
