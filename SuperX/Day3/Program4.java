import java.io.*;
class Demo{

	public static void main(String [] args) throws IOException{
	
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Starting Range : ");
		int startRange= Integer.parseInt(br.readLine());

		System.out.print("Enter Ending Range : ");
		int endRange= Integer.parseInt(br.readLine());

		for(int i= startRange; i<endRange; i++){
		
			int num1= i;
			int rev = 0;
			int t=0;

			while(num1 !=0){
			
				rev= rev*10;
				t= num1%10;
				rev= rev+ t;
				num1= num1/10;
			}
			System.out.println(rev);
		}
	}
}
