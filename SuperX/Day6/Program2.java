import java.io.*;
class Demo{

	public static void main(String [] args )throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());

		int x= 1;
		for(int i= 1; i<= row; i++){
		
			for (int j=1; j<=i; j++){

				if (x==1){
					System.out.print(x);
					x++;
				}
			
				else{
				
				System.out.print(x*x*x-1 +"\t");
				x++;
			}

			}
			System.out.println();
		}
	}
}
