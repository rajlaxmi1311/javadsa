import java.io.*;
class Demo {

	public static void main (String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());

		
		int num= 1;

		
		for(int i=1; i<=row; i++){
		
			char ch= 'A';
			for(int j= 1; j<row ; j++){

				if(i%2==1){
				
					System.out.print(ch +"\t");
					ch++;
				}
				else{
				
					System.out.print( num +"\t");
				num=num+2;
				}
			

			}
			System.out.println();
		}
	}
}
