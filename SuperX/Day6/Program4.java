import java.io.*;
class Demo{

	void Demo(int startRange,int endRange){
	
		if(startRange == endRange)
			return;

		int num= startRange;
		int sum= 0;
	
		while(num!=0){
		int fact= 1;
		int t= num%10;

		for(int i= 1; i<=t;i++){
		
			fact= fact*i;
		} 
		sum= sum+fact;
		
		num= num/10;

		}
		if(startRange== sum){
		
			System.out.println(sum);
		}
		startRange ++;

		Demo(startRange,endRange);

	}

	public static void main(String [] args )throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Starting Range : ");
		int startRange = Integer.parseInt(br.readLine());

		System.out.print("Enter Ending Range : ");
		int endRange = Integer.parseInt(br.readLine());

		Demo obj = new Demo();
		obj.Demo(startRange,endRange);

		
	}
}
