import java.io.*;
class Demo{
	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Number : ");

		int num = Integer.parseInt(br.readLine());
		int LeftMax = Integer.MIN_VALUE;

		int x =0;
		while(num!=0){

			x = num%10;
			if (LeftMax <= x)
				LeftMax = x;

			num = num/10;
		  

		}

		System.out.println(LeftMax);



	}
}
