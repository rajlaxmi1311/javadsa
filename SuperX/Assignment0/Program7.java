import java.io.*;
import java.util.*;
class Demo{
	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print(" Enter Array size : ");

		int arrSize1 = sc.nextInt();
		int arr1[] = new int[arrSize1];

		int arrSize2 = arrSize1;
		int arr2[] = new int[arrSize2];


		System.out.print(" Enter Array Elements :");

		for(int k =0 ; k< arr1.length ; k++){
		
			arr1[k]= sc.nextInt();
		}

		for(int l =0 ; l < arr2.length ; l++){
		
			arr2[l] = 0;
		}


		int i =0;
		int j =0;
		while ( i< arr1.length){
		
			if(arr1[i]%2 == 0){
			
				arr2[j] = 1;
			}
			i++;
			j++;
		}

		System.out.println(Arrays.toString(arr1));

		System.out.println(Arrays.toString(arr2));			
		
	
	
	}
}
