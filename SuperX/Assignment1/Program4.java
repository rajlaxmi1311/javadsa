import java.io.*;
class Demo{

	public static void main (String []args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print(" Enter rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i=1; i<=row; i++) {
		
			for (int j=row-1; j>=i; j--){
			
				System.out.print("\t");
			
			}
			
			for (int k=1; k<=i; k++){
			
				System.out.print(k*i +"\t");
			}
			System.out.println();

		}
	

	}
}
