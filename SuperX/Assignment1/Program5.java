import java.io.*;
class Demo{

	public static void main (String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print(" Start Range : ");
		int startRange = Integer.parseInt(br.readLine());

		System.out.print(" End Range : ");
		int endRange = Integer.parseInt(br.readLine());

		for (int i= startRange ; i <= endRange; i++){
		
			int count =0;
			for(int j=1; j<=i; j++){
			
				if (i%j ==0){
				
					count ++;
				}
			}
			if(count ==2){
			
				System.out.print(i + "\t");
			}
		}
		System.out.println();

	}
}
