import java.io.*;
class Demo{

	public static void main (String []args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows :");
		int row = Integer.parseInt(br.readLine());

		for (int i= 1;i<=row ; i++){
		
			int num= row-1+i;
			for(int j =1; j<=i; j++){
			
				System.out.print(num +"\t");
				num= num+i;
			}
			System.out.println();
		}

	}
}
