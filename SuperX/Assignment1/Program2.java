import java.io.*;
class Demo{

	public static void main (String [] args)throws IOException{
	
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter rows : ");
		int row = Integer.parseInt(br.readLine());
		int num1=2;
		int num2=0;

		for (int i= 1; i<=row ;i++){
		
			for( int j= 1; j<=i; j++){
			
				System.out.print(num1*num2 +"\t");
				num1++;
				num2++;
			}
			System.out.println();
		}
	}
}
