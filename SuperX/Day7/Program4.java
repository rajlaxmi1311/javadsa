import java.io.*;
class Demo{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter starting range : ");
		int startRange = Integer.parseInt(br.readLine());

		System.out.print("Enter ending range : ");
		int endRange = Integer.parseInt(br.readLine());

		for(int i= startRange; i<endRange; i++){
		

			int sum= 0;
			for(int j= 1; j< i; j++){
			
				if(i%j ==0){
				
					sum=sum+j;
				}
			}
			if( i == sum){
			
				System.out.print(i +"\t");
			}
			
		}
		System.out.println();
	}
}
