import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());

		int num1=1;
		int num2=2;

		for(int i= 1; i<=row; i++){
		
			for(int j= 1; j<=row; j++){
			
				if(i%2==1){
				
					System.out.print(num1 +"\t");
					num1= num1+2;
				}
				else{
				
					System.out.print(num2 +"\t");
					num2=num2+2;
				}
			}
			System.out.println();
		}
	} 
}
