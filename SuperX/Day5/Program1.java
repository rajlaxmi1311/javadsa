import java.io.*;
class Demo{

	public static void main (String []args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter starting Range : ");
		int startRange = Integer.parseInt(br.readLine());

		System.out.print("Enter ending Range : ");
		int endRange = Integer.parseInt(br.readLine());

		for (int i= startRange; i<endRange; i++){
		
			int sum=1;
			for(int j = 1;j<=i;j++){

				sum= sum*j;
			}
			System.out.println(sum);
	
		}

	}
}
