import java.io.*;
class Demo{

	public static void main(String [] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter String : ");
		String str = br.readLine();

		System.out.print("Enter Alphabet : ");
		char alpha = (char)br.read();

		int count=0;
		for(int i = 0; i< str.length(); i++){
		
			if(str.charAt(i)== alpha){
			
				count++;
			}

		}
		System.out.println(count);
	}
}
