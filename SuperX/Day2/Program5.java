import java.io.*;
class Demo {

	public static void main(String[]args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter String : ");
		String str = br.readLine();

		int count = 0;

		for (int i =0; i<str.length(); i++){
		
	
			if(str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) =='u'){
			
				count++;
			}
		}
		System.out.println("Count is : " + count);
	}
}
