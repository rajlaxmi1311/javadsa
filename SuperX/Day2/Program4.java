import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Starting Range : ");
		int startRange = Integer.parseInt(br.readLine());

		System.out.print("Enter Ending Range : ");
		int endRange = Integer.parseInt(br.readLine());

		for(int i= startRange; i<= endRange; i++){
		
			int count = 0;
			for(int j= startRange ; j <=i; j++){
			
				if(i%j ==0){
				count++;
				}
			}
			if(count >=3){
			
				System.out.print(i +", ");
			}
		}
	}
}
