import java.io.*;
class Demo{

	public static void main(String [] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Number : ");
		int num= Integer.parseInt(br.readLine());

		int num1=num;
		int rev=0;
		int t=0;

		while(num1!= 0){
		
			rev= rev*10;
			t= num1%10;
			rev= rev+ t;
			num1= num1/10;
		}
		if (num == rev){
		
			System.out.println("True");
		}else{
		
			System.out.println("False");
		}
	}
}
