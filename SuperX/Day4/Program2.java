import java.io.*;
class Demo{

	public static void main(String [] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter no. of rows :");
		int row = Integer.parseInt(br.readLine());

		char ch = 'A';
		for(int i= 1; i<= row; i++){
		
			char ch1=ch;
			for (int j= 1; j<= i; j++){
			
				System.out.print(ch1 +"\t");
				ch1--;

			}
			System.out.println();
			ch++;
		}
	}
}
