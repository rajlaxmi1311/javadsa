//Using Try Catch Methode
// sleep methode
// Exception handling


class MyThread extends Thread{
public void run(){
for( int i=0; i<10; i++ ){
System.out.println("In run");

try{
Thread.sleep(1000);
}
catch( InterruptedException ie ){
}
}
}
}

class ThreadDemo{
public static void main(String [] args) throws InterruptedException{
	MyThread obj = new MyThread();  //thread 0 , new born thread
	obj.start();                    //Start Thred class 0   

	for( int i=0; i<10;i++ ){
	System.out.println("In Main");
	Thread.sleep(1000);
	}

}
}
